@extends('layouts.master')
@section('title', 'Trang Chu')

@section('content_front')
    <div class="container">
        <h1>List of Diffculty</h1>
        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Title</th>
                    <th>Description</th>
                    <th>Created</th>
                    <th class="text-center">Edit</th>
                    <th class="text-center">Delete</th>
                </tr>
            </thead>
            <tbody>
                @for ($i = 0; $i < count($data); $i++)
                    <tr>
                        <td>#{{ $data[$i]['diff_id'] }}</td>
                        <td>{{ $data[$i]['title'] }}</td>
                        <td>{{ $data[$i]['description'] }}</td>
                        <td><?php print date('d-m-Y H:i:s', $data[$i]['created']); ?></td>
                        <td class="text-center"><a class="btn glyphicon glyphicon-pencil" href="{!! route('diff.edit',$data[$i]['diff_id']) !!}"></a></td>
                        <td class="text-center">{!! Form::open(['route' => ['diff.destroy', $data[$i]['diff_id']], 'method' => 'DELETE']) !!}<button type="submit" onclick="return confirm('Chắc chắn muốn xóa không?');" href="{!! route('diff.destroy',$data[$i]['diff_id']) !!}" class="btn glyphicon glyphicon-trash"></button>{!! Form::close() !!}</td>
                    </tr>
                @endfor
                
            </tbody>
        </table>
        <h1>Insert Difficulty</h1>
        {!! Form::open(['route' => 'diff.store', 'method' => 'post']) !!}
            @if(isset($diff_edit[0]['diff_id']))
                {!! Form::hidden('diff_id', $diff_edit[0]['diff_id']) !!}
            @endif
            <div class="form-group">
                <label for="title">Title:</label><br/>
                {!! Form::text('title', '', ['required' => true]) !!}
            </div>
            <div class="form-group">
                <label for="description">Description:</label><br/>
                {!! Form::textarea('description', '', ['required' => true]) !!}
            </div>
            <div class="form-group">
                {!! Form::submit('Save', ['class'=>'btn btn-default']) !!}
            </div>
        {!! Form::close() !!}
    </div>
@stop