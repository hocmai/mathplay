@extends('admin.layout.default')

@section('title')
{{ $title='Danh sách lớp học' }}
@stop

@section('content')
	<table id="grade-list" class="table table-striped table-bordered" cellspacing="1" width="100%">
        <thead>
            <tr>
                <th>STT</th>
                <th>Title</th>
                <th>Mô tả</th>
                <th>Đăng bỏi</th>
                <th>Ngày đăng</th>
                <th>Cập nhật</th>
                <th>Thao tác</th>
            </tr>
        </thead>
        <tbody>
        	@foreach( $data as $grade )
        	<tr>
        		<td>#{{ $loop->index+1 }}</td>
        		<td>{{ $grade['title'] }}</td>
        		<td>{{ $grade['description'] }}</td>
        		<td>{{ $grade->user()->get()->first()->name }}</td>
        		<td style="white-space: nowrap"><?php print date('d-m-Y H:i:s', $grade['created']); ?></td>
        		<td style="white-space: nowrap"><?php print date('d-m-Y H:i:s', $grade['changed']); ?></td>
        		<td style="white-space: nowrap"><button type="button" class="btn btn-info" title="Xem">Xem</button> <button type="button" class="btn btn-primary" title="Sửa"><i class="glyphicon glyphicon-wrench"></i></button> <button type="button" class="btn btn-danger" title="Xóa"><i class="glyphicon glyphicon-trash"></i></button></td>
        	</tr>
        	@endforeach
        </tbody>
    </table>
@stop

@section('js')
<script type="text/javascript">
	$(document).ready(function() {
	    $('#grade-list').DataTable({
	    	"language": {
	            "lengthMenu": "Xem _MENU_ kết quả mỗi trang",
	            "zeroRecords": "Không có dữ liệu.",
	            "info": "Trang _PAGE_/_PAGES_",
	            "infoEmpty": "Không tìm thấy kết quả nào",
	            "infoFiltered": "(Đã lọc từ _MAX_ bản ghi)"
	        }
	    });

    	///////////// Sortable
    	$('#grade-list tbody').sortable({
		    axis: 'y',
		    placeholder: "ui-state-highlight",
		    update: function (event, ui) {
		        var data = $(this).sortable('serialize');
		        console.log(data);

		        // POST to server using $.post or $.ajax
		        // $.ajax({
		        //     data: data,
		        //     type: 'POST',
		        //     url: '/your/url/here'
		        // });
		    }
		});
	});
</script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.15/js/dataTables.bootstrap.min.js"></script>
@stop