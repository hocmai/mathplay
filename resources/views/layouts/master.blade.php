<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title')</title>
    <link href="{{ asset('css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{ asset('css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{ asset('css/hover-min.css')}}" rel="stylesheet">
    <link href="{{ asset('css/slick-theme.css')}}" rel="stylesheet">
    <link href="{{ asset('css/slick.css')}}" rel="stylesheet">
    <link href="{{ asset('css/jssor.css')}}" rel="stylesheet">
    <link href="{{ asset('css/menu.css')}}" rel="stylesheet">
    <link href="{{ asset('css/default.css')}}" rel="stylesheet">
    <link href="{{ asset('css/menu-mobile.css')}}" rel="stylesheet">
    <link href="{{ asset('css/style.css')}}" rel="stylesheet">

    <script src="{{ asset('js/jquery.min.js')}}"></script>
    <script src="{{ asset('js/bootstrap.min.js')}}"></script>

    <script src="{{ asset('js/slick.min.js')}}" ></script>
    <script src="{{ asset('js/menu-mobile.js')}}" ></script>

    <script src="{{ asset('js/main.js')}}"></script>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.min.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div id="full_page" class="full_page">
    <div class="menu-mobile header-content clearfix hidden-lg">
        <a class="logo" href="#" title="">
            <img src="{{ asset('images/logo.png')}}" class="img-responsive" alt=""/>
        </a>
        <div id="button" class="button"></div>
        <ul class="header-menu-left no-list-style">
            <li>
                <a class="dang-nhap hvr-shadow" data-toggle="modal" data-target="#myModal-dang-nhap" href="#" title="">Đăng nhập <span class="fa fa-key"></span></a>
            </li>
            <li>
                <a class="dang-ky hvr-shadow" data-toggle="modal" data-target="#myModal-dang-ky" href="#" title="">Đăng ký <span class="fa fa-lock"></span></a>
            </li>

            <li><a href="index.php" title="Trang chủ">Trang chủ</a></li>
            <li>
                <a href="#" title="">Lớp học</a>
                <ul>
                    <li><a href="#" title="">Lớp học 1</a></li>
                    <li><a href="#" title="">Lớp học 2</a></li>
                </ul>
            </li>
            <li><a href="#" title="">Giới thiệu</a></li>
            <li><a href="#" title="">Liên hệ</a></li>
        </ul>
    </div>

    <header class="hidden-xs hidden-sm">
        <div class="box-dang-nhap">
            <div class="container">

                <a class="dang-ky hvr-shadow" href="dang-ky.html" title="">Đăng ký</a>
                <a class="dang-nhap hvr-shadow" data-toggle="modal" data-target="#myModal-dang-nhap" href="#" title="">Đăng nhập</a>

            </div>

        </div>
        <div class="box-menu">
            <div class="container">
                <a class="logo" href="#" title="">
                    <img src="{{ asset('images/logo.png')}}" class="img-responsive" alt=""/>
                </a>
                <div class="menu">
                    <div class="rmm style">
                        <ul class="fl hidden-xs hidden-sm">
                            <li class="active">
                                <a href="index.html">
                                    <div class="bg-hv"><span></span></div>
                                    Trang chủ
                                </a>
                            </li>
                            <li>
                                <a href="lop-hoc.html">
                                    <div class="bg-hv"><span></span></div>
                                    Lớp học
                                </a>
                                <ul>
                                    <li><a href="lop-hoc.html">Lớp 1</a></li>
                                    <li><a href="lop-hoc.html">Lớp 2</a></li>
                                    <li class="bg-color"></li>
                                </ul>
                            </li>
                            <li>
                                <a href="index.html">
                                    <div class="bg-hv"><span></span></div>
                                    Giới thiệu
                                </a>
                            </li>
                            <li>
                                <a href="index.html">
                                    <div class="bg-hv"><span></span></div>
                                    Liên hệ
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <div class="bracum">
        <div class="container">
            <ol class="bracum-mobile">
                <li><a href="#">Trang chủ</a></li>
            </ol>
        </div>
    </div>

    @section('slide')
    @show
<main>

    @yield('content_front')

</main>
<footer>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="des">
                    Cơ quan chủ quản: Công ty Cổ phần Đầu tư và Dịch vụ Giáo dục <br/>
                    Địa chỉ: Tầng 4, Tòa nhà 25T2, Đường Nguyễn Thị Thập, Phường Trung Hoà, Quận Cầu Giấy, Hà Nội.<br/>
                    Tel: +84 (4) 3519-0591 Fax: +84 (4) 3519-0587<br/>
                    Giấy phép cung cấp dịch vụ mạng xã hội trực tuyến số 597/GP-BTTTT Bộ Thông tin và Truyền thông cấp ngày 30/12/2016.<br/>
                </div>
            </div>
        </div>
    </div>
    <div class="leg">
        <div class="container">
            <div class="des"><img src="{{ asset('images/a-cong.png')}}" class="img-responsive" alt=""/>2017 phát triển và xây dựng bởi HOCMAI</div>
            <div class="social">
                <a href="" title=""><img src="{{ asset('images/f.png')}}" class="img-responsive" alt=""/></a>
                <a href="" title=""><img src="{{ asset('images/g.png')}}" class="img-responsive" alt=""/></a>
            </div>
        </div>

    </div>
</footer>
<div id="lof_go_top">
    <span class="ico_up"></span>
</div>

    <!-- Modal -->
    <div class="modal fade" id="myModal-dang-nhap" tabindex="-1" role="dialog" aria-labelledby="myModalLabel-dang-nhap">
        <div class="modal-dialog" role="document">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="hidden-lg hidden-md text-center">Đăng nhập</h4>
            </div>
            <div class="modal-content">
                <div class="box-chuong-trinh box-dang-ky">
                    <div class="row m0">
                        <div class="col-sm-12">
                            <div class="row m0 bg-ff">
                                <div class="col-sm-6 bor">
                                    <form>
                                        <div class="chuong-trinh">
                                            <div class="form" >
                                                <div class="form-group">
                                                    <input type="text" class="form-control" placeholder="Tên đăng nhập">
                                                </div>
                                                <div class="form-group">
                                                    <input type="password" class="form-control" placeholder="Mật khẩu">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox"> Nhớ tài khoản
                                            </label>
                                        </div>
                                        <div class="form-group button">
                                            <button type="submit" class="btn btn-default">Đăng nhập</button>
                                        </div>
                                    </form>
                                </div>
                                <div class="col-sm-6">
                                    <div class="boxs-right">
                                        <div class="chuong-trinh">
                                            <div class="des">
                                                Bạn có thể đăng nhập bằng tài khoản mạng xã hội
                                            </div>

                                            <div class="form" >
                                                <div class="box-dang-nhap-khac">
                                                    <a class="facebook" href="#" title="">Đăng nhập bằng Facebook</a>
                                                    <a class="google" href="#" title="">Đăng nhập bằng Google</a>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                </div>
                                <div class="col-sm-12 hidden-lg hidden-md">
                                    <div class="thanh-vien">
                                        Chưa có tài khoản, <a href="" title="">bấm vào đây để đăng ký</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="myModal-dang-ky" tabindex="-1" role="dialog" aria-labelledby="myModalLabel-dang-ky">
        <div class="modal-dialog" role="document">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4>Đăng ký tài khoản</h4>
            </div>
            <div class="modal-content">
                <div class="box-chuong-trinh box-dang-ky">
                    <div class="row m0">
                        <div class="col-sm-12">
                            <div class="row m0 bg-ff">
                                <div class="col-sm-12 bor">
                                    <form>
                                        <div class="chuong-trinh">
                                            <div class="form" >
                                                <div class="form-group">
                                                    <input type="text" class="form-control" placeholder="Tên đăng nhập">
                                                </div>
                                                <div class="form-group">
                                                    <input type="text" class="form-control" placeholder="Họ tên đầy đủ">
                                                </div>
                                                <div class="form-group">
                                                    <input type="password" class="form-control" placeholder="Mật khẩu truy">
                                                </div>
                                                <div class="form-group">
                                                    <input type="password" class="form-control" placeholder="Nhập lại mật khẩu">
                                                </div>
                                                <div class="form-group">
                                                    <input type="email" class="form-control" placeholder="Email">
                                                </div>
                                                <div class="form-group">
                                                    <input type="text" class="form-control" placeholder="Số điện thoại">
                                                </div>

                                            </div>
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox"> Nhớ tài khoản
                                            </label>
                                        </div>
                                        <div class="form-group button">
                                            <button type="submit" class="btn btn-default">Đăng ký</button>
                                        </div>
                                    </form>
                                </div>
                                <div class="col-sm-12 hidden-lg hidden-md">
                                    <div class="thanh-vien">
                                        Nếu bạn đã là thành viên, <a href="" title="">bấm vào đây để đăng nhập</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

</body>
</html>