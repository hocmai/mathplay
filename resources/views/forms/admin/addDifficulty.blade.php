@extends('layouts.master')
@section('title', 'Trang Chu')
@section('content_front')
    <div class="container">
        <h1>Edit Difficulty</h1>
        {!! Form::open(['route' => ['diff.update', $data['diff_id']], 'method' => 'PUT']) !!}
            <div class="form-group">
                <label for="title">Title:</label><br/>
                {!! Form::text('title', $data['title'], ['required' => true]) !!}
            </div>
            <div class="form-group">
                <label for="description">Description:</label><br/>
                {!! Form::textarea('description', $data['description'], ['required' => true]) !!}
            </div>
            <div class="form-group">
                {!! Form::submit('Save', ['class'=>'btn btn-default']) !!}
            </div>
        {!! Form::close() !!}
    </div>
@stop