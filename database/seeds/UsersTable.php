<?php

use Illuminate\Database\Seeder;

class UsersTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        $limit = 5;

        for ($i = 0; $i < $limit; $i++) {
            DB::table('users')->insert([ //,
                'name' => $faker->userName,
                'email' => $faker->unique()->email,
                'password' => md5(123456),
                'created' => time()+$i+5,
                'changed' => time()+$i+5,
            ]);
        }
    }
}
