<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLessionDifficultTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lession_question', function (Blueprint $table) {
            $table->integer('lession_id');
            $table->integer('qid');
            $table->binary('config')->nullable();
        });

        Schema::table('grade', function (Blueprint $table) {
            $table->primary(['lession_id', 'qid']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lession_question');
    }
}
