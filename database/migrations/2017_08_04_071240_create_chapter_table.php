<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChapterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chapter', function (Blueprint $table) {
            $table->increments('chapter_id');
            $table->string('title', 225);
            $table->longText('description')->nullable();
            $table->integer('subject_id')->unsigned();
            $table->integer('author')->unique();
            $table->string('slug', 255)->unique();
            $table->integer('created')->nullable();
            $table->integer('changed')->nullable();
        });

        Schema::table('grade', function (Blueprint $table) {
            $table->foreign('subject_id')->references('subject_id')->on('subject');
            $table->foreign('author')->references('id')->on('users');
            $table->index(['subject_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chapter');
    }
}
