<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudyHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('study_history', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('uid')->unsigned();
            $table->integer('grade_id');
            $table->integer('subject_id');
            $table->integer('chapter_id')->nullable();
            $table->integer('lession_id')->nullable();
            $table->integer('difficult')->nullable();
            $table->integer('created')->nullable();
            $table->integer('time_use')->nullable();
        });

        Schema::table('grade', function (Blueprint $table) {
            $table->foreign('uid')->references('id')->on('users');
            $table->foreign('grade_id')->references('grade_id')->on('users');
            $table->foreign('subject_id')->references('subject_id')->on('subject');
            $table->foreign('chapter_id')->references('chapter_id')->on('chapter');
            $table->foreign('lession_id')->references('lession_id')->on('lession');
            $table->index(['uid', 'grade_id', 'subject_id', 'chapter_id', 'lession_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('study_history');
    }
}
