<?php
$namespace = 'Modules\question\Controllers';
Route::group(
    ['prefix' => 'group', 'module'=>'question', 'namespace' => $namespace],
    function() {
        Route::get('test', [
            # middle here
            'as' => 'index',
            'uses' => 'QuestionController@index'
        ])->name('home');
    }
);
Route::get('/{grade}/{subject}', function($grade, $subject){
    echo 'lop:'. $grade.'</br>Mon hoc: '.$subject;
});