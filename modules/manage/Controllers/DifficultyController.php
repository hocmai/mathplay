<?php
namespace Modules\manage\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Helper\Url;

class DifficultyController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index()
    {
        //return Url::test('heheheh');
        return view('front::index');
    }
}
