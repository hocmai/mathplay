<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/manage', [
    'as' => 'index',
    'uses' => 'QuestionController@index'
])->name('manage');

View::composer(['manage'], function($view){
    return $view->with('title', 'Manage difficulty');
});
