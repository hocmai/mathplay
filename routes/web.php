<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

View::composer(['home'], function($view){
    return $view->with('title', 'Welcome my page!');
});
// Route::resource('/test','ManageDiff');
// Route::controller('ManageDiff');

// Route::get('/manage', 'ManageDiff@formrender')->name('diff_form');

// Route::group(
//     ['prefix' => '/diff/{diff_id}'],
//     function() {
//         Route::get('edit', [
//             # middle here
//             'as' => 'EditDifficulty',
//             'uses' => 'ManageDiff@editdiff'
//         ]);
//         Route::get('delete', [
//             # middle here
//             'as' => 'DeleteDifficulty',
//             'uses' => 'ManageDiff@deletediff'
//         ]);
//     }
// );

//Route::get('/diff/{diff_id}/edit', 'ManageDiff@formrender')->name('diff_edit_form')->where('diff_id', '[0-9]+');


//Route::post('/manage/save', ['as' => 'SaveDifficulty', 'uses' => 'ManageDiff@formsubmit']);

Route::resource('diff', 'DiffAd');


Route::group(['prefix' => 'admin'], function(){
	Route::get('grade', 'Admin\GradeController@index');
});