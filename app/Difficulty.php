<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Difficulty extends Model
{
    protected $table = 'difficulty';
    protected $primaryKey = 'diff_id';

    protected $filltable = ['*'];
    public $timestamps = false;
}