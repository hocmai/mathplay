<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Difficulty;

class DiffAd extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Difficulty::all();
        return view('manage', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $diff = new Difficulty;
        $diff->title = $request->title;
        $diff->description = $request->description;
        $diff->author = 1;
        $diff->created = time();
        $diff->changed = time();
        $diff->save();

        //Difficulty::create([]);
        return redirect()->route('diff.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Difficulty::findOrFail($id);
        return view('forms.admin.addDifficulty', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Difficulty::findOrFail($id);
        return view('forms.admin.addDifficulty', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $diff = Difficulty::findOrFail($id);
        $diff->title = $request->title;
        $diff->description = $request->description;
        $diff->changed = time();
        $diff->save();

        return redirect()->route('diff.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $diff = Difficulty::findOrFail($id);
        $diff->delete();
        return redirect()->route('diff.index');
    }
}
