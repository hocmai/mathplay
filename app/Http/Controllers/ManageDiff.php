<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Difficulty;

class ManageDiff extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function formrender()
    {
    	$diff = new Difficulty;
    	//print '<pre>';
    	//print_r($diff::all()->toArray());
    	//print '</pre>';
    	$data = $diff::all()->toArray();

        return view('manage', ['data' => $data]);
    }



    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function formsubmit(Request $request)
    {
		$diff = new Difficulty;
		$diff->title = $request->title;
		$diff->description = $request->description;
		$diff->author = 1;
		$diff->created = time();
		$diff->changed = time();
		$diff->save();

		// Difficulty::create([])
        return redirect('/manage');
    }



    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function editdiff($diff_id)
    {
		$diff = new Difficulty;
    	//print '<pre>';
    	//print_r($diff::all()->toArray());
    	//print '</pre>';
    	$data = $diff::all()->toArray();
    	$diff_edit = $diff::find(['diff_id' => $diff_id])->toArray();
    	

        return view('manage', ['data' => $data, 'diff_edit' => $diff_edit]);
    }



    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function deletediff(Request $request)
    {
        return 'delete';
    }
}
