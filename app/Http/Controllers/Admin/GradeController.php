<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\GradeModel;

class GradeController extends Controller
{
    public function index(){
    	$data = GradeModel::all();
    	//dd($data);
    	return view('admin.grade.grade_list', compact('data'));
    }
}

