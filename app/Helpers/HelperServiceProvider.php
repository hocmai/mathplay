<?php
namespace CustomHelper;

use Illuminate\Support\ServiceProvider;
use File;

class HelpServiceProvider extends  ServiceProvider{
    public function boot(){
        $listHelper = array_map('basename', File::directories(__DIR__));
        foreach ($listHelper as $helper) {
            foreach (glob(app_path().'/Helpers/'.$helper.'/*.php') as $filename){
                require_once($filename);
            }
        }
    }
    public function register(){}
}